import unittest

class TestSort(unittest.TestCase):

  sample = [10, 8 ,1, 3, 5, 8, 4, 9]

  def test_ascending(self):
    sample_sorted = sorted(self.sample)
    for i, value in enumerate(sample_sorted):
      if i != len(sample_sorted) - 1:
        self.assertTrue(value <= sample_sorted[i+1])

  def test_descendig(self):
    sample_sorted = sorted(self.sample, reverse=True)
    for i, value in enumerate(sample_sorted):
      if i != len(sample_sorted) - 1:
        self.assertTrue(value >= sample_sorted[i+1])

if __name__ == '__main__':
  unittest.main()
