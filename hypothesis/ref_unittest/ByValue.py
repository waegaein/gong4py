import unittest

class TestStringMethods(unittest.TestCase):

  def test_upper(self):
    self.assertEqual('foo'.upper(), 'FOO')

  def test_isupper(self):
    self.assertTrue('FOO'.isupper())
    self.assertFalse('Foo'.isupper())

  def test_split(self):
    s = 'hello world'
    self.assertEqual(s.split(), ['hello', 'world'])
    with self.assertRaises(TypeError):
      s.split(2)

class TestDivide(unittest.TestCase):

  def test_divide_by_10(self):
    self.assertEqual(20/10, 2)

  def test_divide_by_0(self):
    with self.assertRaises(ZeroDivisionError):
      20/0

if __name__ == '__main__':
  unittest.main()
