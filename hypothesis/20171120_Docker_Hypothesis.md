# Docker

## Docker를 활용한 독립 개발 환경 만들기
2017-11-20, 김동석

---

### I. 필요성
*   Windows 10 Laptop
*   로컬 파일 시스템과 완전히 독립적인 환경에서 작업하고 싶다. (Zero Side-effect)
*   커맨드라인에 익숙해지고 싶다.
*   Powershell의 인터페이스가 불편하다.
*   하지만 VM은 무겁고 느리다.

### II. Container
*   (Official) Package software into standardized units for development, shipment and deployment
*   Virtualization that is ...
 -   Stand-alone
 -   Isolated
 -   Lightweight

### III. Docker
*   (Official) Docker is the company driving the container movement and the only container platform provider to address every application across the hybrid cloud.
*   Container Management System (Like DBMS for DB!)

### IV. 설치 (for Windows)
<https://docs.docker.com/docker-for-windows/install/>

### V. 실행
*   데몬은 부팅과 함께 자동으로 실행
*   DOCKERFILE로부터 이미지 생성 **docker build -t *REPO:TAG PATH_TO_DOCKERFILE***
*   로컬에 있는 이미지 확인 **docker images**
*   이미지로부터 컨테이너 생성 **docker run -dit *IMAGE_NAME***
*   컨테이너 실행 **docker start *CONTAINER_ID***
*   컨테이너 종료 **docker stop *CONTAINER_ID***
*   실행 중인 컨테이너 확인 **docker ps**
*   실행 중인 / 종료 된 컨테이너 확인 **docker ps -a**
*   실행 중인 컨테이너에 Shell에 연결 **docker exec -it *CONTAINER_ID* bash**

### Ex

```bash
PS C:\Users\waegaein\docker> ls

Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----     2017-11-18   오후 6:09                dockerfile

PS C:\Users\waegaein> docker images
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
waegaein/ubuntu1604python36   latest              733b9ef0fc93        29 hours ago        637MB
ubuntu                        16.04               20c44cd7596f        40 hours ago        123MB
PS C:\Users\waegaein> docker ps -a
CONTAINER ID        IMAGE                         COMMAND             CREATED             STATUS                      PORTS               NAMES
1b8b1a1f4371        waegaein/ubuntu1604python36   "/bin/bash"         28 hours ago        Exited (0) 48 seconds ago                       gong4py
PS C:\Users\waegaein> docker start gong4py
gong4py
PS C:\Users\waegaein> docker ps
CONTAINER ID        IMAGE                         COMMAND             CREATED             STATUS              PORTS                NAMES
1b8b1a1f4371        waegaein/ubuntu1604python36   "/bin/bash"         28 hours ago        Up 6 seconds       0.0.0.0:80->80/tcp   gong4py
PS C:\Users\waegaein> docker exec -it gong4py bash
root@1b8b1a1f4371:/# ls
bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
root@1b8b1a1f4371:/# echo lalala
lalala
```

---

---

# Hypothesis

## Hypothesis를 활용한 Python Unit Testing
2017-11-20, 김동석

---

### I. Why unittest sucks...

#### unittest
*   White-box Testing: 코드, 특히 함수의 Correctness를 확인하는 용도
*   assertEqauls, assertTrue, assertRaises 등을 활용하여, 함수의 실행 결과가 특정 값 또는 조건을 만족하는지 확인
*   각 테스트는 서로 독립적으로 실행되므로, 실행 순서에 상관 없이 작동하도록 구성

#### Ex1

```python
import unittest

class TestStringMethods(unittest.TestCase):

  def test_upper(self):
    self.assertEqual('foo'.upper(), 'FOO')

  def test_isupper(self):
    self.assertTrue('FOO'.isupper())
    self.assertFalse('Foo'.isupper())

  def test_split(self):
    s = 'hello world'
    self.assertEqual(s.split(), ['hello', 'world'])
    with self.assertRaises(TypeError):
      s.split(2)

class TestDivide(unittest.TestCase):

  def test_divide_by_10(self):
    self.assertEqual(20/10, 2)

  def test_divide_by_0(self):
    with self.assertRaises(ZeroDivisionError):
      20/0

if __name__ == '__main__':
  unittest.main()
```
```bash
root@1b8b1a1f4371:~/gong4py/hypothesis/ref_unittest# py ByValue.py -v
test_divide_by_0 (__main__.TestDivide) ... ok
test_divide_by_10 (__main__.TestDivide) ... ok
test_isupper (__main__.TestStringMethods) ... ok
test_split (__main__.TestStringMethods) ... ok
test_upper (__main__.TestStringMethods) ... ok

----------------------------------------------------------------------
Ran 5 tests in 0.003s

OK
```

*   단점
 -  정상적인 Input/Output을 정확히 알아야 한다. 일반적인 경우에는 괜찮을 수 있다.
 -  (assertRaises) Edge-case를 정확히 알아야 한다. 테스트를 작성하는 시점에서는 아직 Correctness를 모르는 상태인데, 테스트로는 파악이 안된다.

#### Ex2

```python
import unittest

class TestSort(unittest.TestCase):

  sample = [10, 8 ,1, 3, 5, 8, 4, 9]

  def test_ascending(self):
    sample_sorted = sorted(self.sample)
    for i, value in enumerate(sample_sorted):
      if i != len(sample_sorted) - 1:
        self.assertTrue(value <= sample_sorted[i+1])

  def test_descendig(self):
    sample_sorted = sorted(self.sample, reverse=True)
    for i, value in enumerate(sample_sorted):
      if i != len(sample_sorted) - 1:
        self.assertTrue(value >= sample_sorted[i+1])

if __name__ == '__main__':
  unittest.main()
```
```bash
root@1b8b1a1f4371:~/gong4py/hypothesis/ref_unittest# py ByProperty.py -v
test_ascending (__main__.TestSort) ... ok
test_descendig (__main__.TestSort) ... ok

----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

*   단점
 -  Input이 복잡한 데이터인 경우, 일일이 만드는게 너무 귀찮다.

#### 결론
*   Input 만들기 귀찮다.
*   코드 수정 전에 확인 차 돌려보는 Smoke Testing, 코드 변경 후에 여전히 잘 돌아가는지 확인하는 Regression Testing으로는 괜찮지만, Correctness Testing을 하기에는 부족하다. 이를 위해 필요한 Edge-case는 사람이 열심히 찾아야 한다.
*   테스트를 위한 테스트가 되기 쉽다.

---

### II. Hypothesis is here to rescue!

#### Hypothesis
*   (Official) Hypothesis is a Python library for creating unit tests which are simpler to write and more powerful when run, finding Edge-cases in your code you wouldn’t have thought to look for. It is stable, powerful and easy to add to any existing test suite.
*   Input을 자동으로 만들어 주어서 덜 귀찮다.
*   Input을 다양하게 만들어 주어서 Edge-case를 찾기 쉽다.
*   기존의 unittest와 함께 사용하기 쉽다.

#### Ex1-1: 그냥 돌려본다.

```python
import unittest
from hypothesis import given
import hypothesis.strategies as st

class TestDivide(unittest.TestCase):

  @given(n1=st.integers(), n2=st.integers())
  def test_exception(self, n1, n2):
    n1/n2

if __name__ == '__main__':
  unittest.main()
```
```bash
root@1b8b1a1f4371:~/gong4py/hypothesis/ex1_test# py ByValue.py -v
test_exception (__main__.TestDivide) ... Falsifying example: test_exception(self=<__main__.TestDivide testMethod=test_exception>, n1=0, n2=0)
You can add @seed(125060325262537449754819073295620912226) to this test to reproduce this failure.
ERROR

======================================================================
ERROR: test_exception (__main__.TestDivide)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "ByValue.py", line 7, in test_exception
    def test_exception(self, n1, n2):
  File "/usr/local/lib/python3.5/dist-packages/hypothesis/core.py", line 867, in wrapped_test
    state.run()
  File "/usr/local/lib/python3.5/dist-packages/hypothesis/core.py", line 699, in run
    print_example=True, is_final=True
  File "/usr/local/lib/python3.5/dist-packages/hypothesis/executors.py", line 58, in default_new_style_executor
    return function(data)
  File "/usr/local/lib/python3.5/dist-packages/hypothesis/core.py", line 136, in run
    return test(*args, **kwargs)
  File "ByValue.py", line 7, in test_exception
    def test_exception(self, n1, n2):
  File "/usr/local/lib/python3.5/dist-packages/hypothesis/core.py", line 457, in timed_test
    result = test(*args, **kwargs)
  File "ByValue.py", line 8, in test_exception
    n1/n2
ZeroDivisionError: division by zero

----------------------------------------------------------------------
Ran 1 test in 0.024s

FAILED (errors=1)
```

*   Edge-case를 찾아준다 (... Falsifying example: ... n1=0, n2=0)
*   Edge-case를 재현할 수 있다 (... add @seed(~~~) to this test ...)

#### Ex1-2: 앞서 찾은 Edge-case를 Input Boundary로 하는 테스트를 추가한다.

```python
@given(n1=st.integers(min_value=1), n2=st.integers())
def test_exceptionN1_1(self, n1, n2):
  n1/n2

@given(n1=st.integers(max_value=-1), n2=st.integers())
def test_exceptionN1_2(self, n1, n2):
  n1/n2

@given(n1=st.integers(), n2=st.integers(min_value=1))
def test_exceptionN2_1(self, n1, n2):
  n1/n2

@given(n1=st.integers(), n2=st.integers(max_value=-1))
def test_exceptionN2_2(self, n1, n2):
  n1/n2
```
```bash
root@1b8b1a1f4371:~/gong4py/hypothesis/ex1_test# py ByValue.py -v
test_exception (__main__.TestDivide) ... Falsifying example: test_exception(self=<__main__.TestDivide testMethod=test_exception>, n1=0, n2=0)
You can add @seed(171900112332345416524060985903979789534) to this test to reproduce this failure.
ERROR
test_exceptionN1_1 (__main__.TestDivide) ... Falsifying example: test_exceptionN1_1(self=<__main__.TestDivide testMethod=test_exceptionN1_1>, n1=1, n2=0)
You can add @seed(302934307671667531413257853548643485645) to this test to reproduce this failure.
ERROR
test_exceptionN1_2 (__main__.TestDivide) ... Falsifying example: test_exceptionN1_2(self=<__main__.TestDivide testMethod=test_exceptionN1_2>, n1=-1, n2=0)
You can add @seed(302934307671667531413257853548643485645) to this test to reproduce this failure.
ERROR
test_exceptionN2_1 (__main__.TestDivide) ... ok
test_exceptionN2_2 (__main__.TestDivide) ... ok
```

*   n1 != 0일 때 여전히 실패하지만, n2 != 0일 때 성공한다.
*   따라서 Edge-case는 n2=0이며, 유일하다.

#### Ex1-3: Edge-case Handling

```python
@given(n1=st.integers(), n2=st.integers())
def test_divide(self, n1, n2):
  if n2 == 0:
    with self.assertRaises(ZeroDivisionError):
      n1/n2
  else:
    n1/n2
```
```bash
root@1b8b1a1f4371:~/gong4py/hypothesis/ex1_test# py ByValue.py -v
test_divide (__main__.TestDivide) ... ok

----------------------------------------------------------------------
Ran 1 test in 0.119s

OK
```
#### Ex2: List of integers
```python
@given(sample=st.lists(st.integers(), average_size=100, max_size=150))
def test_ascending(self, sample):
  sample_sorted = sorted(sample)
  for i, value in enumerate(sample_sorted):
    if i != len(sample_sorted) - 1:
      self.assertTrue(value <= sample_sorted[i+1])
```

#### 결론
*   unittest의 기능을 모두 사용할 수 있다.
*   테스트로 Edge-case를 파악할 수 있다.
*   Smoke Testing + Regression Testing + **Correctness Testing** = 진짜 Unit Testing

---

### +α

어떤 Input이 들어갈까?

```python
@given(n1=st.integers(), n2=st.integers())
def test_divide(self, n1, n2):
  print('n1: ' + str(n1) + ' n2: ' + str(n2))
  if n2 == 0:
    with self.assertRaises(ZeroDivisionError):
      n1/n2
  else:
    n1/n2
```
```bash
n1: 0 n2: 0
n1: 0 n2: 1
n1: -62049669237089570798501778236177284177 n2: -109645057733144684720960711409228341845
n1: -146051457986507458796319299413304851042 n2: -101285099308148467305374392854927009543
n1: -5056306190852009895508529049681348427 n2: 137737172422931389617687444607187560977
n1: -20842532685735915888379776000678207510 n2: 3589305834574914157502186325075216843
n1: 53901489231337192185563162339427068074 n2: 18188668562517059779851939842056710752
n1: 89680605482089894645462137852389348722 n2: -100140870939474275939710259410529527931
n1: 2699126125644563437943162552526055186 n2: -30662704066146687997488160683412113600
n1: 29168074378859479394638575790035336300 n2: -161249758169155951516638884740581955927
n1: 154959379716278486420728158673189786005 n2: -56244692529432879582624558615341822981
n1: 4134040128824887642574540768495066778 n2: 73445754281196472853140588419775466816
n1: 0 n2: 2
n1: -170141183460469231731687303715884105727 n2: 0
n1: 0 n2: -106712679550642508014563239954429855776
n1: 8796093022208 n2: -106712679550642526904029171433010710560
n1: 83076749736557242056496737360543744 n2: -106712679550641317978209556803836004384
n1: -106712679550641317978209556803836004384 n2: 83076749736557242056496737360543744
n1: -71353182869404384761613870599901608000 n2: 0
n1: 75951380340556093109809378405844681593 n2: 0
n1: 75951380340556093109809378405844681593 n2: 1
n1: -107020867492795410356573353375307661262 n2: 75951380340556093109809378405844681593
n1: 75951380340556093109809378405844681593 n2: -107020867492795410356573353375307661262
n1: 75951380340556093109809378405844681593 n2: -161145544099901599286286207212656812148
n1: 75951380340556093109809378405844681593 n2: -103347526679889133097795197418160147608
n1: -103150170317793818130442331792487996125 n2: -22552187190636144683476800171160303674
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105727
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105472
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105473
n1: 15640097518558346380019125647978978710 n2: 81608602861099985038095825142202364698
n1: 15640097518558950842928932962566331798 n2: 0
n1: 8964293810763343595718423931941341696 n2: 0
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105474
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105475
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105476
n1: 149880319307742851985915692310507138874 n2: -170141183460469231731687303715884105727
n1: 149880319307742851985915692310507138874 n2: -55930416790162614436475540669817827427
n1: -55930416790162614436475540669817827427 n2: 149880319307742851985915692310507138874
n1: -55930416790162614436475540669817827427 n2: 149880319307742851985915692310507138816
n1: -55930416790162614436475540669817827427 n2: 0
n1: -55930415522512014208246139173114622051 n2: -55930416790162614436475540669817827427
n1: -57707426097539990949501894985937735662 n2: -65528864606610211216743128868933022079
n1: -152117191156852397977981042178829467737 n2: -161162664045176650269231283463980589739
n1: -152117191156852397977980901441341112409 n2: -152773915275620001406035981591328128217
n1: -157434103139992061469596129682462490713 n2: -152117191156852397977980901441341112409
n1: 0 n2: 3
n1: -63385931093115988292197230796113686447 n2: 0
n1: 0 n2: 4
n1: 42702100946941297375796029167908167712 n2: 0
n1: 42702100946941297375796029167908167712 n2: 1
n1: 106244755546255146083338977221337826471 n2: -93816611153414058466340663305293477687
n1: 33239040071853371027603107117214919142 n2: -93816611153414059056636473663999129399
n1: 33239019789443767375932683169963633126 n2: 45370982256125128461783280990902428194
n1: 0 n2: 5
n1: 0 n2: 6
n1: 0 n2: 7
n1: 0 n2: 8
n1: 0 n2: 9
n1: 0 n2: 10
n1: 0 n2: 41538374868278621028243970633760778
n1: -76861229842851885807422343259211678557 n2: 0
n1: -76861229842851885807422343259211662173 n2: 32
n1: 0 n2: 11
n1: -170141183460469231731687303715884105727 n2: 1
n1: 1 n2: -170141183460469231731687303715884105727
n1: 0 n2: 139994319790098926401014696002914364404
n1: -164849321675715981571514493827881750537 n2: -119928070712548759011492192676753908954
n1: -164849321675715981571514493827881750537 n2: 38364461703802754452755498907711887283
n1: -15575573014213359364526472403819091143 n2: -164849321675715981571514493827881750537
n1: -108537664138757826595336231968275949780 n2: -126830061807878820318502863123939264650
n1: -108537664138757826595336231968275949780 n2: 108257228172633925359246324280983241652
n1: -108537664138757826595336231968271755476 n2: 160517498434166873506812095617237007014
n1: -108537664138757826595336231968271755476 n2: 94745286476026003552547439716296247111
n1: -108537664138757826595336231968271755476 n2: -108537664138757826595336231968271755476
n1: 0 n2: 49948526194953598290725000443533718594
n1: 0 n2: 12
n1: 0 n2: 13
n1: -143452370368630920871814785485941500907 n2: 106800601076680010686922340917982220178
n1: -143452370368630920871814785485941500907 n2: -6004982945663619943471316601737086084
n1: -170141183460469231731687303715884105727 n2: -16680508182398944287420323893714128012
n1: -170141183460469231731687303715884103679 n2: -170141183460469231731687303715884105727
n1: 0 n2: 14
n1: 0 n2: 15
n1: 15 n2: -170141183460469231731687303715884105727
n1: 15 n2: 15
n1: 0 n2: 16
n1: 0 n2: 17
n1: 0 n2: -39234688091006382482960344329113103107
n1: -170141183460469231731687303715884105727 n2: -170141183460469231731687303715884105477
n1: -170141183460469231731687303715884105727 n2: 103142675230889967269148868919180478252
n1: -69628125915581658757876634796829153506 n2: -33610034128711750621708975815708189284
n1: -69628125915581658757876634796829153506 n2: -69628125915581658757876634796829153506
n1: 0 n2: 12981058928631027099281737559408298064
n1: 12981058928631027099281737559408298064 n2: 12981058928631027099281737559408298064
n1: 12981058928631027099281737559408298064 n2: -76296437080631070377334719120392494505
n1: -145155608959638868213368258429780556099 n2: 0
n1: -145155608959638868213368258429780556099 n2: -136780167095671343156846655928455849702
n1: -145155608959638868213368258429780556099 n2: -136780167095671343156846655928455849472
n1: -145155608959638868213368258429780556099 n2: 63924944159560121952199678220099386225
n1: -145155608959638868213368258429780556099 n2: -170141183460469231731687303715884105727
```

