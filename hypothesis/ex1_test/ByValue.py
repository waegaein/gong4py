import unittest
from hypothesis import given
import hypothesis.strategies as st

class TestDivide(unittest.TestCase):

  @given(n1=st.integers(), n2=st.integers())
  def test_exception(self, n1, n2):
    n1/n2

  @given(n1=st.integers(min_value=1), n2=st.integers())
  def test_exceptionN1_1(self, n1, n2):
    n1/n2
  
  @given(n1=st.integers(max_value=-1), n2=st.integers())
  def test_exceptionN1_2(self, n1, n2):
    n1/n2

  @given(n1=st.integers(), n2=st.integers(min_value=1))
  def test_exceptionN2_1(self, n1, n2):
    n1/n2

  @given(n1=st.integers(), n2=st.integers(max_value=-1))
  def test_exceptionN2_2(self, n1, n2):
    n1/n2

  @given(n1=st.integers(), n2=st.integers())
  def test_divide(self, n1, n2):
    if n2 == 0:
      with self.assertRaises(ZeroDivisionError):
        n1/n2
    else:
      n1/n2

if __name__ == '__main__':
  unittest.main()
