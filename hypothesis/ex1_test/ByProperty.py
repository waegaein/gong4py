import unittest
from hypothesis import given
import hypothesis.strategies as st

class TestSort(unittest.TestCase):

  @given(sample=st.lists(st.integers(), average_size=100, max_size=150))
  def test_ascending(self, sample):
    sample_sorted = sorted(sample)
    for i, value in enumerate(sample_sorted):
      if i != len(sample_sorted) - 1:
        self.assertTrue(value <= sample_sorted[i+1])

  @given(sample=st.lists(st.integers(), average_size=100, max_size=150))
  def test_descendig(self, sample):
    sample_sorted = sorted(sample, reverse=True)
    for i, value in enumerate(sample_sorted):
      if i != len(sample_sorted) - 1:
        self.assertTrue(value >= sample_sorted[i+1])

if __name__ == '__main__':
  unittest.main()
